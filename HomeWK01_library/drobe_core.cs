﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWK01_classes
{
    public partial class drobes
    {
        //Поля класса
        protected internal int Znamenat;
        protected internal int Chislit;

        //Делегат и событие
        public delegate void DrobeChange(int num);
        public event DrobeChange Change;
        public int ZnamenatPrev
        {
            get
            {
                return Znamenat;
            }

            set
            {
                
                if (Change != null)
                {
                    Change(value);
                }
                Znamenat = value;
            }
        }
        public int ChsilitPrev
        {
            get
            {
                return Chislit;
            }

            set
            {
                if (Change != null)
                    Change(value);

                Chislit = value;
            }
        }

        //Индексатор
        public int this[int index]
        {
            get
            {
                if (index == 0)
                {
                    return Znamenat;
                }

                if (index == 1)
                {
                    return Chislit;
                }

                return -1;
            }
        }
        //Конструкторы
        public drobes(int Chislit)
        {
            this.Chislit = Chislit;
            this.Znamenat = 1; 
        }
        public drobes(int Chislit, int Znamenat)
        {
            this.Chislit = Chislit;
            this.Znamenat = Znamenat;
        }
        public drobes(int value, int Chislit, int Znamenat) 
        { 
            this.Chislit = value * Znamenat + Chislit;
            this.Znamenat = Znamenat;
        }
        
        //операторы
        public static drobes operator +(drobes value, drobes value1)
        {
            NOCdown.NOC(value, value1);
            return new drobes(
                value.Chislit + value1.Chislit
                , value.Znamenat = value1.Znamenat
                );
        }
        public static drobes operator -(drobes value, drobes value2)
        {
            NOCdown.NOC(value, value2);
            return new drobes(
                value.Chislit - value2.Chislit
                , value.Znamenat = value2.Znamenat
                );
        }
        public static drobes operator /(drobes value, drobes value1)
        {
            return new drobes(
                value.Chislit * value1.Znamenat,
                value.Znamenat * value1.Chislit
                );
        }
        public static drobes operator *(drobes value, drobes value1)
        {
            return new drobes(
                value1.Chislit * value.Chislit,
                value1.Znamenat * value.Znamenat
                );
        }
        
        public int ReturnZnamenat()
        {
            return this.Znamenat;
        }

        public int ReturnChislit()
        {
            return this.Chislit;
        }
    }
}
